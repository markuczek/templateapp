package com.apps4u.templateapp.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps4u.templateapp.R;
import com.apps4u.templateapp.model.dto.News;
import com.apps4u.templateapp.model.dto.PriceListItem;

public class PricesListAdapter extends ArrayAdapter<PriceListItem> {
	private final Context context;
	private final PriceListItem[] values;

	public PricesListAdapter (Context context, PriceListItem[] values) {
		super(context, R.layout.row_price, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout rowView = (LinearLayout)inflater.inflate(R.layout.row_price, parent, false);

		TextView itemName_TextView = (TextView) rowView.findViewById(R.id.menu_item_name);
		TextView itemPrice_TextView = (TextView) rowView.findViewById(R.id.menu_item_price);

		itemName_TextView.setText(values[position].getName());
		itemPrice_TextView.setText(values[position].getPrice());
			
		return rowView;
	}
}
