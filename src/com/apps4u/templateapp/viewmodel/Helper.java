package com.apps4u.templateapp.viewmodel;

import java.util.List;

import com.apps4u.templateapp.model.dto.Company;
import com.apps4u.templateapp.model.dto.News;
import com.apps4u.templateapp.model.dto.Opinion;
import com.apps4u.templateapp.model.dto.PriceListItem;
import com.apps4u.templateapp.model.dto.SpecialOffer;
import com.apps4u.templateapp.model.dto.Info;
import com.apps4u.templateapp.model.enums.AccountType;

public class Helper {
	
	public static final long CompanyID = 1234;
	
	public static String FeedAddress = "http://visittorun.cba.pl/";
	
	public static AccountType AccountType = com.apps4u.templateapp.model.enums.AccountType.Inactive;

	public static Company Company;

	public static SpecialOffer SpecialOffer;

	public static List<Opinion> OpinionList;

	public static List<News> NewsList;

	public static List<PriceListItem> PricesList;
	
	public static List<String> Images;

	public static Info Info = new Info();
	
}
