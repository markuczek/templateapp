package com.apps4u.templateapp.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps4u.templateapp.R;
import com.apps4u.templateapp.model.dto.Opinion;

public class OpinionListAdapter extends ArrayAdapter<Opinion> {
	private final Context context;
	private final Opinion[] values;

	public OpinionListAdapter (Context context, Opinion[] values) {
		super(context, R.layout.row_opinion, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout rowView = (LinearLayout)inflater.inflate(R.layout.row_opinion, parent, false);
		
		TextView itemComment_TextView = (TextView) rowView.findViewById(R.id.opinion);
		TextView itemDate_TextView = (TextView) rowView.findViewById(R.id.opinion_date);
		
		itemComment_TextView.setText(values[position].getComment());
		itemDate_TextView.setText(values[position].getDate());
			
		return rowView;
	}
}
