package com.apps4u.templateapp.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps4u.templateapp.R;
import com.apps4u.templateapp.model.dto.News;

public class NewsListAdapter extends ArrayAdapter<News> {
	private final Context context;
	private final News[] values;

	public NewsListAdapter (Context context, News[] values) {
		super(context, R.layout.row_news, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout rowView = (LinearLayout)inflater.inflate(R.layout.row_news, parent, false);

		TextView itemTitle_TextView = (TextView) rowView.findViewById(R.id.news_title);
		TextView itemDate_TextView = (TextView) rowView.findViewById(R.id.news_date);
		TextView itemMessage_TextView = (TextView) rowView.findViewById(R.id.news_message);

		itemTitle_TextView.setText(values[position].getTitle());
		itemDate_TextView.setText(values[position].getDate());
		itemMessage_TextView.setText(values[position].getMessage());
			
		return rowView;
	}
}
