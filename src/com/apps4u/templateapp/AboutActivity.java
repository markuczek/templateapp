package com.apps4u.templateapp;

import com.apps4u.templateapp.viewmodel.Helper;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		populate();
	}

	private void populate() {
		TextView name = (TextView) findViewById(R.id.about_name);
		name.setText(Helper.Company.getName());

		TextView content = (TextView) findViewById(R.id.about_content);
		content.setText(Helper.Company.getAbout());

		ImageView logo = (ImageView) findViewById(R.id.about_logo);

        ImageLoader.getInstance().displayImage(Helper.Company.getLogoUrl(), logo);
		
	}
	
}
