package com.apps4u.templateapp;

import com.apps4u.templateapp.viewmodel.Helper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class InfoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);

		populate();
	}

	private void populate() {
//		StringBuilder sb = new StringBuilder();
//		sb.append(Helper.Company.getCity());
//		sb.append(System.getProperty("line.separator"));
//		sb.append(getResources().getString(R.string.street));
//		sb.append(Helper.Company.getStreet());
		
		TextView title = (TextView) findViewById(R.id.info_title);
		title.setText(Helper.Info.getName());
		
		TextView about = (TextView) findViewById(R.id.info_about_content);
		about.setText(Helper.Info.getAbout());
		
		TextView adres = (TextView) findViewById(R.id.info_facebook);
		adres.setText(Helper.Info.getFacebook());

		TextView phone = (TextView) findViewById(R.id.info_phone);
		phone.setText(Helper.Info.getPhone());

		TextView mail = (TextView) findViewById(R.id.info_mail);
		mail.setText(Helper.Info.getMail());

		TextView www = (TextView) findViewById(R.id.info_www);
		www.setText(Helper.Info.getWWW());

        if(Helper.Info.getPhone().equals("")){
        	((FrameLayout) findViewById(R.id.info_phone_frame)).setVisibility(View.INVISIBLE);
        }
        if(Helper.Info.getWWW().equals("")){
        	((FrameLayout) findViewById(R.id.info_www_frame)).setVisibility(View.INVISIBLE);
        }
        if(Helper.Info.getMail().equals("")){
        	((FrameLayout) findViewById(R.id.info_mail_frame)).setVisibility(View.INVISIBLE);
        }
		
	}
	
	public boolean facebookButton_Click(View v) {
		try {
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setData(Uri.parse(Helper.Info.getFacebook()));
	        
	        startActivity(intent);
	        
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
        return true;
    }
	
	public boolean phoneButton_Click(View v) {
		
		try {
	        Intent intent = new Intent(Intent.ACTION_CALL);
	        intent.setData(Uri.parse("tel:"+Helper.Info.getPhone()));
	        startActivity(intent);
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
        return true;
    }
	
	public boolean wwwButton_Click(View v) {
		try {
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setData(Uri.parse(Helper.Info.getWWW()));
	        
	        startActivity(intent);
	        
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
		
	return true;
	}
	
	public boolean mailButton_Click(View v) {

		try {
	        Intent intent = new Intent(Intent.ACTION_SEND);
	        intent.setType("message/rfc822");
	        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{Helper.Info.getMail()});

	        startActivity(Intent.createChooser(intent, "Send Email"));

	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
	return true;
	}

}
