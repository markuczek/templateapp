package com.apps4u.templateapp;

import com.apps4u.templateapp.viewmodel.Helper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ContactActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);

		populate();
	}

	private void populate() {
		StringBuilder sb = new StringBuilder();
		sb.append(Helper.Company.getCity());
		sb.append(System.getProperty("line.separator"));
		sb.append(getResources().getString(R.string.street));
		sb.append(Helper.Company.getStreet());
		
		TextView adres = (TextView) findViewById(R.id.adress);
		adres.setText(sb.toString());

		TextView phone = (TextView) findViewById(R.id.phone);
		phone.setText(Helper.Company.getPhone());

		TextView mail = (TextView) findViewById(R.id.mail);
		mail.setText(Helper.Company.getMail());

		TextView www = (TextView) findViewById(R.id.www);
		www.setText(Helper.Company.getWWW());

        if(Helper.Company.getPhone().equals("")){
        	((FrameLayout) findViewById(R.id.phone_frame)).setVisibility(View.INVISIBLE);
        }
        if(Helper.Company.getWWW().equals("")){
        	((FrameLayout) findViewById(R.id.www_frame)).setVisibility(View.INVISIBLE);
        }
        if(Helper.Company.getMail().equals("")){
        	((FrameLayout) findViewById(R.id.mail_frame)).setVisibility(View.INVISIBLE);
        }
		
	}

	
	public boolean adressButton_Click(View v) {
		
		//mPager.setCurrentItem(3, true);
		
        return true;
    }
	
	public boolean phoneButton_Click(View v) {
		
		try {
	        Intent intent = new Intent(Intent.ACTION_CALL);
	        intent.setData(Uri.parse("tel:"+Helper.Company.getPhone()));
	        startActivity(intent);
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
        return true;
    }
	
	public boolean wwwButton_Click(View v) {
		try {
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setData(Uri.parse(Helper.Company.getWWW()));
	        
	        startActivity(intent);
	        
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
		
	return true;
	}
	
	public boolean mailButton_Click(View v) {

		try {
	        Intent intent = new Intent(Intent.ACTION_SEND);
	        intent.setType("message/rfc822");
	        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{Helper.Company.getMail()});

	        startActivity(Intent.createChooser(intent, "Send Email"));

	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
	return true;
	}
}
