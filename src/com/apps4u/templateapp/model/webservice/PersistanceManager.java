package com.apps4u.templateapp.model.webservice;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.apps4u.templateapp.MainActivity;
import com.apps4u.templateapp.dialogs.NoConnectionDialog;
import com.apps4u.templateapp.model.dto.PriceListItem;
import com.apps4u.templateapp.viewmodel.Helper;

public class PersistanceManager {
	
	public static boolean saveOpinion(String msg){
		boolean result = true;
		
		// TODO
		
		return result;
	}

	public static void retriveData(MainActivity activity) {
		
		DataProvider provider = new DataProvider();
        provider.execute(activity);
		
		List<PriceListItem> prices = new ArrayList<PriceListItem>();
		prices.add(new PriceListItem("Margarita", "15 z�"));
		prices.add(new PriceListItem("Vesuvio", "18 z�"));
		prices.add(new PriceListItem("Capriciosa", "19 z�"));
		
		Helper.PricesList = prices;
		
	}

	public static boolean isConnectionAvailable(Activity activity) {
		
		ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
	    
	    if (networkInfo != null && networkInfo.isConnected()) {
	    	return true;
	    } else {
			// if no internet connection show dialog with information about that and return
	    	if(activity != null)
	    		NoConnectionDialog.create(activity);
	    	
	    	return false;
	    }
	}

}
