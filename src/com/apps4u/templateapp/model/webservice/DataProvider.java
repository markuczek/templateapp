package com.apps4u.templateapp.model.webservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.apps4u.templateapp.MainActivity;
import com.apps4u.templateapp.viewmodel.Helper;

public class DataProvider extends AsyncTask{

	private static final String mRemoteFileName = "users/1234/feed/feed_1234.xml";//"retrive_data.php";
	private static final String mFileName = "feed.xml";
	private MainActivity mMainActivity;
    
	// onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(Object result) {
    	
    	// if you cannot load it means that there are no data in memory (you need to download it again)
    	if(!loadData())
    		PersistanceManager.isConnectionAvailable(mMainActivity);
    	
    	if(mMainActivity != null)
    		mMainActivity.updateView();
    }
    
	@Override
	protected Object doInBackground(Object... params) {
		try {

			if(params.length == 1 && params[0] instanceof MainActivity){
				mMainActivity = (MainActivity) params[0];
			}
			
			return downloadData();
			
	    } catch (IOException e) {
	        return null;
	    }
	}
	
	
	private boolean downloadData() throws IOException {

		InputStream in = null;
		FileOutputStream out = null;
	        
	    try {
	        URL url = new URL(Helper.FeedAddress + mRemoteFileName);// + "?id=" + Helper.CompanyID);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setReadTimeout(60000 /* milliseconds*/ );
	        conn.setConnectTimeout(60000 /* milliseconds*/ );
	        conn.setUseCaches(true);
	        conn.addRequestProperty("Content-Type", "text/xml; charset=utf-8");
	        conn.setRequestMethod("GET");
	        
	        // Starts the query
	        conn.connect();
	        int response = conn.getResponseCode();

	        String UTF8 = "utf8";
			int BUFFER_SIZE = 8192;
				
			out = mMainActivity.openFileOutput(mFileName, Context.MODE_PRIVATE);
			in = conn.getInputStream();
				
			BufferedReader br = new BufferedReader(new InputStreamReader(in, UTF8),BUFFER_SIZE);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out, UTF8),BUFFER_SIZE);
				
			int mByte;
			int lines = 0;	
			while((mByte = br.read()) != -1){
			   	bw.write(mByte);
			   	lines++;
			}
				
			bw.flush();
			bw.close();
			br.close();
	        
			return true;
	        
	    }catch(Exception ex){
			return false;
		} 
	    
	    finally {
	        if (in != null) {
	            try {
					in.close();
				}catch (IOException e) {}
	        } 
	        if (out != null) {
	            try {
					out.close();
				}catch (IOException e) {}
	        }
	    }
	}

	public boolean loadData() {
		
		try {
            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            
            FeedXMLHandler saxHandler = new FeedXMLHandler();
            
            xmlReader.setContentHandler(saxHandler);

            FileInputStream in = mMainActivity.openFileInput(mFileName);
            xmlReader.parse(new InputSource(in));
            //xmlReader.parse(new InputSource(conn.getInputStream()));
            Helper.AccountType = saxHandler.getAccountType();
            Helper.Company = saxHandler.getCompany();
            Helper.SpecialOffer = saxHandler.getSpecialOffer();
            Helper.OpinionList = saxHandler.getOpinionList();
            Helper.NewsList = saxHandler.getNewsList();
            Helper.PricesList = saxHandler.getPricesList();
            Helper.Images = saxHandler.getImagesList();
 
        } catch (Exception ex) {
            Log.d("XML", "SAXXMLParser: parse() failed. " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
		
		return true;
	}
}
