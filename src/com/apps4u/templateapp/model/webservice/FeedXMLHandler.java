package com.apps4u.templateapp.model.webservice;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.apps4u.templateapp.model.dto.Company;
import com.apps4u.templateapp.model.dto.News;
import com.apps4u.templateapp.model.dto.Opinion;
import com.apps4u.templateapp.model.dto.PriceListItem;
import com.apps4u.templateapp.model.dto.SpecialOffer;
import com.apps4u.templateapp.model.enums.AccountType;
 
public class FeedXMLHandler extends DefaultHandler {

	private AccountType mAccountType;

	private Company mCompany;

	private SpecialOffer mSpecialOffer;

	private List<Opinion> mOpinionList;

	private List<News> mNewsList;

	private List<PriceListItem> mPricesList;

	private List<String> mImagesList;
	
    private String tempVal;

	private Opinion mOpinion;

	private News mNews;
	
 
    public FeedXMLHandler() {}
 
    
    public List<Opinion> getOpinionList() {
        return mOpinionList;
    }
    public List<News> getNewsList() {
        return mNewsList;
    }
    public List<PriceListItem> getPricesList() {
        return mPricesList;
    }
    public List<String> getImagesList() {
        return mImagesList;
    }
    public AccountType getAccountType() {
        return mAccountType;
    }
    public Company getCompany() {
        return mCompany;
    }
    public SpecialOffer getSpecialOffer() {
        return mSpecialOffer;
    }
 
    // Event Handlers
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        // reset
        tempVal = "";
        if (qName.equalsIgnoreCase("Company")) {
        	mCompany = new Company();
        } else if (qName.equalsIgnoreCase("SpecialOffer")) {
        	mSpecialOffer = new SpecialOffer();
        } else if (qName.equalsIgnoreCase("PricesList")) {
        	mPricesList = new ArrayList<PriceListItem>();
        } else if (qName.equalsIgnoreCase("NewsList")) {
        	mNewsList = new ArrayList<News>();
        } else if (qName.equalsIgnoreCase("OpinionList")) {
        	mOpinionList = new ArrayList<Opinion>();
        } else if (qName.equalsIgnoreCase("ImageList")) {
        	mImagesList = new ArrayList<String>();
        } else if (qName.equalsIgnoreCase("Opinion")) {
        	mOpinion = new Opinion();
        } else if (qName.equalsIgnoreCase("News")) {
        	mNews = new News();
        }
    }
 
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        tempVal = new String(ch, start, length);
    }
 
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
    	
    	if (qName.equalsIgnoreCase("AccountType")) {
    		if(tempVal.equals("0"))
    			mAccountType = AccountType.Inactive;
    		else if(tempVal.equals("1"))
    			mAccountType = AccountType.Basic;
    		else if(tempVal.equals("2"))
    			mAccountType = AccountType.Standard;
    		else if(tempVal.equals("3"))
    			mAccountType = AccountType.Premium;
        }else if (qName.equalsIgnoreCase("Id")) {
        	mCompany.setID(Long.parseLong(tempVal));
        }else if (qName.equalsIgnoreCase("Name")) {
        	mCompany.setName(tempVal);
        }else if (qName.equalsIgnoreCase("About")) {
        	mCompany.setAbout(tempVal);
        }else if (qName.equalsIgnoreCase("Lat")) {
        	mCompany.setLat(tempVal);
        }else if (qName.equalsIgnoreCase("Lng")) {
        	mCompany.setLng(tempVal);
        }else if (qName.equalsIgnoreCase("City")) {
        	mCompany.setCity(tempVal);
        }else if (qName.equalsIgnoreCase("Street")) {
        	mCompany.setStreet(tempVal);
        }else if (qName.equalsIgnoreCase("Facebook")) {
        	mCompany.setFacebook(tempVal);
        }else if (qName.equalsIgnoreCase("Phone")) {
        	mCompany.setPhone(tempVal);
        }else if (qName.equalsIgnoreCase("Www")) {
        	mCompany.setWWW(tempVal);
        }else if (qName.equalsIgnoreCase("Mail")) {
        	mCompany.setMail(tempVal);
        }else if (qName.equalsIgnoreCase("LogoUrl")) {
        	mCompany.setLogoUrl(tempVal);
        }else if (qName.equalsIgnoreCase("ImageUrl")) {
        	mImagesList.add(tempVal);
        }else if (qName.equalsIgnoreCase("Opinion")) {
        	mOpinionList.add(mOpinion);
        }else if (qName.equalsIgnoreCase("OpinionComment")) {
        	mOpinion.setComment(tempVal);
        }else if (qName.equalsIgnoreCase("OpinionLanguage")) {
        	mOpinion.setLanguage(tempVal);
        }else if (qName.equalsIgnoreCase("OpinionDate")) {
        	mOpinion.setDate(tempVal);
        }else if (qName.equalsIgnoreCase("News")) {
        	mNewsList.add(mNews);
        }else if (qName.equalsIgnoreCase("NewsTitle")) {
        	mNews.setTitle(tempVal);;
        }else if (qName.equalsIgnoreCase("NewsMessage")) {
        	mNews.setMessage(tempVal);
        }else if (qName.equalsIgnoreCase("NewsLanguage")) {
        	mNews.setLanguage(tempVal);
        }else if (qName.equalsIgnoreCase("NewsDate")) {
        	mNews.setDate(tempVal);
        }else if (qName.equalsIgnoreCase("OfferUrl")) {
        	mSpecialOffer.setOfferUrl(tempVal);
        }else if (qName.equalsIgnoreCase("offerImageURL")) {
        	mSpecialOffer.setImageUrl(tempVal);
        }else if (qName.equalsIgnoreCase("OfferTitle")) {
        	mSpecialOffer.setTitle(tempVal);
        }
    }
}