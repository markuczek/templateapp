package com.apps4u.templateapp.model.dto;

public class PriceListItem {
	private String mName;
	private String mPrice;
	
	public PriceListItem() {
	}
	
	public PriceListItem(String name, String price) {
		this.mName = name;
		this.mPrice = price;
	}

	public String getName() {
		return mName;
	}
	public String getPrice() {
		return mPrice;
	}


	public void setName(String val) {
		mName = val;
	}
	public void setPrice(String val) {
		mPrice = val;
	}
}
