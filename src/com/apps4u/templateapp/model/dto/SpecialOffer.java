package com.apps4u.templateapp.model.dto;

public class SpecialOffer {
	
	private String mImageUrl = "";
	private String mTitle = "";
	private String mOfferUrl = "";

	public SpecialOffer(){}
	
	public SpecialOffer(String url){
		mImageUrl = url;
	}
	
	public SpecialOffer(String url, String msg){
		mImageUrl = url;
		mTitle = msg;
	}
	
	public SpecialOffer(String url, String msg, String offerUrl){
		mImageUrl = url;
		mTitle = msg;
		mOfferUrl = offerUrl;
	}

	public void setImageUrl(String url){
		mImageUrl = url;
	}
	public void setTitle(String msg){
		mTitle = msg;
	}
	public void setOfferUrl(String url){
		mOfferUrl = url;
	}
	
	public String getImageUrl(){
		return mImageUrl;
	}
	public String getTitle(){
		return mTitle;
	}
	public String getOfferUrl() {
		return mOfferUrl;
	}

}
