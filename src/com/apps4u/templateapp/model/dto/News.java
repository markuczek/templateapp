package com.apps4u.templateapp.model.dto;

public class News {
	private String mTitle;
	private String mMessage;
	private String mLanguage;
	private String mDate;
	
	public News() {
	}
	
	public News(String title, String message, String lng, String date) {
		this.mTitle = title;
		this.mMessage = message;
		this.mLanguage = lng;
		this.mDate = date;
	}

	public String getTitle() {
		return mTitle;
	}
	public String getMessage() {
		return mMessage;
	}
	public String getLanguage() {
		return mLanguage;
	}
	public String getDate() {
		return mDate;
	}


	public void setTitle(String val) {
		mTitle = val;
	}
	public void setMessage(String val) {
		mMessage = val;
	}
	public void setDate(String val) {
		mDate = val;
	}
	public void setLanguage(String val) {
		mLanguage = val;
	}
}
