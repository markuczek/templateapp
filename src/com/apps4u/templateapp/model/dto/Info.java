package com.apps4u.templateapp.model.dto;


public class Info {

	private String mName;
	private String mPhone;
	private String mWWW;
	private String mMail;
	private String mAbout;
	private String mFacebook;
	private String mLogoUrl;

	public Info() {
	
		mName = "GoApp";
		mPhone = "+48 514 331 524";
		mWWW = "http://GoAppMobile.eu";
		mMail = "info@GoAppMobile.com";
		mAbout = "Je�eli chcesz mie� tak� apk� skontaktuj si� z nami. Dla nas klient jest zawsze na pierwszym miejscu!";
		mFacebook = "http://facebook.com/pages/Jacenty-P1/162875033759733";
		mLogoUrl = "http://visittorun.cba.pl/users/1234/images/logo.png";
	}

	public void setName(String name){
		mName = name;
	}
	public void setPhone(String phone){
		mPhone = phone;
	}
	public void setWWW(String www){
		mWWW = www;
	}
	public void setMail(String mail){
		mMail = mail;
	}
	public void setAbout(String about){
		mAbout = about;
	}
	public void setFacebook(String facebook){
		mFacebook = facebook;
	}
	public void setLogoUrl(String url){
		mLogoUrl = url;
	}

	public String getName(){
		return mName;
	}
	public String getPhone(){
		return mPhone;
	}
	public String getWWW(){
		return mWWW;
	}
	public String getMail(){
		return mMail;
	}
	public String getAbout(){
		return mAbout;
	}
	public String getFacebook(){
		return mFacebook;
	}
	public String getLogoUrl(){
		return mLogoUrl;
	}
	

}
