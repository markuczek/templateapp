package com.apps4u.templateapp.model.dto;

public class Opinion {
	private String mComment;
	private String mLanguage;
	private String mDate;
	
	public Opinion() {
	}
	
	public Opinion(String comment, String lng, String date) {
		this.mComment = comment;
		this.mLanguage = lng;
		this.mDate = date;
	}

	public String getComment() {
		return mComment;
	}
	public String getLanguage() {
		return mLanguage;
	}
	public String getDate() {
		return mDate;
	}


	public void setComment(String val) {
		mComment = val;
	}
	public void setDate(String val) {
		mDate = val;
	}
	public void setLanguage(String val) {
		mLanguage = val;
	}
}
