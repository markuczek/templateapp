package com.apps4u.templateapp.model.dto;


public class Company {

	private long mID;
	private String mName;
	private String mCity;
	private String mStreet;
	private String mLat;
	private String mLng;
	private String mPhone;
	private String mWWW;
	private String mMail;
	private String mAbout;
	private String mFacebook;
	private String mLogoUrl;

	public void setID(long id){
		mID = id;
	}
	public void setName(String name){
		mName = name;
	}
	public void setCity(String city){
		mCity = city;
	}
	public void setStreet(String street){
		mStreet = street;
	}
	public void setLat(String lat){
		mLat = lat;
	}
	public void setLng(String lng){
		mLng = lng;
	}
	public void setPhone(String phone){
		mPhone = phone;
	}
	public void setWWW(String www){
		mWWW = www;
	}
	public void setMail(String mail){
		mMail = mail;
	}
	public void setAbout(String about){
		mAbout = about;
	}
	public void setFacebook(String facebook){
		mFacebook = facebook;
	}
	public void setLogoUrl(String url){
		mLogoUrl = url;
	}

	public long getID(){
		return mID;
	}
	public String getName(){
		return mName;
	}
	public String getCity(){
		return mCity;
	}
	public String getStreet(){
		return mStreet;
	}
	public String getLat(){
		return mLat;
	}
	public String getLng(){
		return mLng;
	}
	public String getPhone(){
		return mPhone;
	}
	public String getWWW(){
		return mWWW;
	}
	public String getMail(){
		return mMail;
	}
	public String getAbout(){
		return mAbout;
	}
	public String getFacebook(){
		return mFacebook;
	}
	public String getLogoUrl(){
		return mLogoUrl;
	}
	

}
