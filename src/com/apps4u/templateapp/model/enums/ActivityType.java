package com.apps4u.templateapp.model.enums;

public enum ActivityType {

	AboutActivity, ContactActivity, GalleryActivity, MainActivity, MapActivity, OpinionsActivity 
}
