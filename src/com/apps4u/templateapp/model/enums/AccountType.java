package com.apps4u.templateapp.model.enums;

public enum AccountType {
	Inactive, Basic, Standard, Premium
}
