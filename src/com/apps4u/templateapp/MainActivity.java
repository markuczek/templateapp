package com.apps4u.templateapp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.apps4u.templateapp.model.webservice.PersistanceManager;
import com.apps4u.templateapp.viewmodel.Helper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    
        // configure Image Loader
        configureImageLoader();

        // lockInput
        disableMenu();
        
        // update data and retrive account level
        updateData();
	}

	public boolean info_onClick(View v){
		Intent intent = new Intent(this, InfoActivity.class);
    	startActivity(intent);
		return false;
	}

	// O nas
	public boolean menu1_onClick(View v){
		Intent intent = new Intent(this, AboutActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Oferta specjalna
	public boolean menu2_onClick(View v){
		Intent intent = new Intent(this, SpecialOfferActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Kontakt
	public boolean menu3_onClick(View v){
		Intent intent = new Intent(this, ContactActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Galeria
	public boolean menu4_onClick(View v){
		Intent intent = new Intent(this, GalleryActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Cennik
	public boolean menu5_onClick(View v){
		Intent intent = new Intent(this, PricesActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Mapa
	public boolean menu6_onClick(View v){
		Intent intent = new Intent(this, MapActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Aktualności
	public boolean menu7_onClick(View v){
		Intent intent = new Intent(this, NewsActivity.class);
    	startActivity(intent);
		return false;
	}
	
	// Facebook
	public boolean menu8_onClick(View v){
		try {
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setData(Uri.parse(Helper.Company.getFacebook()));
	        
	        startActivity(intent);
	        
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
		
	return true;
	}
	
	// Opinie
	public boolean menu9_onClick(View v){
		Intent intent = new Intent(this, OpinionsActivity.class);
    	startActivity(intent);
		return false;
	}


	private void configureImageLoader() {

		// Create global configuration and initialize ImageLoader with this configuration
		
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .cacheOnDisc(true)
        .build();
		
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
        .defaultDisplayImageOptions(defaultOptions)
        .build();
        
        ImageLoader.getInstance().init(config);
	}
	
	private void updateData() {
	    
		findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
	    
		PersistanceManager.retriveData(this);
	}

	public void updateView() {
        // load logo
		loadLogo();
        
        // enable/disable menu icons
        applyAccountType();
        
        // disable progress bar
	    findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
	}

	private void loadLogo() {
		ImageView logo = (ImageView) findViewById(R.id.main_logo);

		if(Helper.Company != null)
			ImageLoader.getInstance().displayImage(Helper.Company.getLogoUrl(), logo);
	}

	private void applyAccountType() {
		switch (Helper.AccountType) {
		case Inactive:
			findViewById(R.id.menu1).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu2).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu3).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu4).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu5).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu6).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu7).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu8).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu9).setVisibility(View.INVISIBLE);
			
			findViewById(R.id.service_unavailable_textbox).setVisibility(View.VISIBLE);
			break;
			
		case Basic:
			findViewById(R.id.menu1).setVisibility(View.VISIBLE);
			findViewById(R.id.menu2).setVisibility(View.VISIBLE);
			findViewById(R.id.menu3).setVisibility(View.VISIBLE);
			findViewById(R.id.menu4).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu5).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu6).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu7).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu8).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu9).setVisibility(View.INVISIBLE);
			
			findViewById(R.id.service_unavailable_textbox).setVisibility(View.INVISIBLE);
			break;
			
		case Standard:
			findViewById(R.id.menu1).setVisibility(View.VISIBLE);
			findViewById(R.id.menu2).setVisibility(View.VISIBLE);
			findViewById(R.id.menu3).setVisibility(View.VISIBLE);
			findViewById(R.id.menu4).setVisibility(View.VISIBLE);
			findViewById(R.id.menu5).setVisibility(View.VISIBLE);
			findViewById(R.id.menu6).setVisibility(View.VISIBLE);
			findViewById(R.id.menu7).setVisibility(View.VISIBLE);
			findViewById(R.id.menu8).setVisibility(View.VISIBLE);
			findViewById(R.id.menu9).setVisibility(View.VISIBLE);
			
			findViewById(R.id.service_unavailable_textbox).setVisibility(View.INVISIBLE);
			break;
			
		case Premium:
			findViewById(R.id.menu1).setVisibility(View.VISIBLE);
			findViewById(R.id.menu2).setVisibility(View.VISIBLE);
			findViewById(R.id.menu3).setVisibility(View.VISIBLE);
			findViewById(R.id.menu4).setVisibility(View.VISIBLE);
			findViewById(R.id.menu5).setVisibility(View.VISIBLE);
			findViewById(R.id.menu6).setVisibility(View.VISIBLE);
			findViewById(R.id.menu7).setVisibility(View.VISIBLE);
			findViewById(R.id.menu8).setVisibility(View.VISIBLE);
			findViewById(R.id.menu9).setVisibility(View.VISIBLE);
			
			findViewById(R.id.service_unavailable_textbox).setVisibility(View.INVISIBLE);
			break;

		default:
			findViewById(R.id.menu1).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu2).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu3).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu4).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu5).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu6).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu7).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu8).setVisibility(View.INVISIBLE);
			findViewById(R.id.menu9).setVisibility(View.INVISIBLE);
			
			findViewById(R.id.service_unavailable_textbox).setVisibility(View.VISIBLE);
			break;
		}
	}

	private void disableMenu() {
		findViewById(R.id.menu1).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu2).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu3).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu4).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu5).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu6).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu7).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu8).setVisibility(View.INVISIBLE);
		findViewById(R.id.menu9).setVisibility(View.INVISIBLE);
	}
}
