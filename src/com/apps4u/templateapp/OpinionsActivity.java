package com.apps4u.templateapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.apps4u.templateapp.model.dto.Opinion;
import com.apps4u.templateapp.model.webservice.PersistanceManager;
import com.apps4u.templateapp.viewmodel.Helper;
import com.apps4u.templateapp.viewmodel.OpinionListAdapter;

public class OpinionsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opinions);
		
		updateOpinionsContent();
	}
	
	
	public boolean sendOpinion_onClick(View v){

		TextView opinion = (TextView)findViewById(R.id.my_opinion);
		
		PersistanceManager.saveOpinion(opinion.getText().toString());
    	
    	return false;
	}

	public void updateOpinionsContent() {
		if (Helper.OpinionList == null || Helper.OpinionList.size() == 0)
			return;

		final Opinion[] opinions = new Opinion[Helper.OpinionList.size()];
		Helper.OpinionList.toArray(opinions);

		OpinionListAdapter adapter = new OpinionListAdapter(this, opinions);
		
		ListView scrollView = (ListView)findViewById(R.id.opinion_list);
		scrollView.setAdapter(adapter);
	}

}
