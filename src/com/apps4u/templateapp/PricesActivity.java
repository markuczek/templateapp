package com.apps4u.templateapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.apps4u.templateapp.model.dto.PriceListItem;
import com.apps4u.templateapp.viewmodel.Helper;
import com.apps4u.templateapp.viewmodel.PricesListAdapter;

public class PricesActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prices);
		
		updatePricesContent();
	}

	public void updatePricesContent() {
		if (Helper.PricesList == null || Helper.PricesList.size() == 0)
			return;

		final PriceListItem[] prices = new PriceListItem[Helper.PricesList.size()];
		Helper.PricesList.toArray(prices);

		PricesListAdapter adapter = new PricesListAdapter(this, prices);
		
		ListView scrollView = (ListView)findViewById(R.id.prices_list);
		scrollView.setAdapter(adapter);
	}

}
