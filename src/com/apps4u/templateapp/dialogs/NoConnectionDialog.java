package com.apps4u.templateapp.dialogs;

import com.apps4u.templateapp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class NoConnectionDialog {

	public static void create(final Activity activity){
			
		AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
		dialog.setTitle(R.string.connection_dialog_title);
		dialog.setMessage(R.string.connection_dialog_message);

		dialog.setPositiveButton(R.string.connection_dialog_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
            	dialog.dismiss();
            }
        });
		
		dialog.show();
	}
}
