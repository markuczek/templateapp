package com.apps4u.templateapp;

import com.apps4u.templateapp.model.webservice.PersistanceManager;
import com.apps4u.templateapp.viewmodel.Helper;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialOfferActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_special_offer);
	}
	
	@Override
	protected void onResume() {
		
		updateSpecialOffer();
		
		super.onResume();
	}
	
	public boolean specialOffer_onClick(View v){
		try {
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setData(Uri.parse(Helper.SpecialOffer.getOfferUrl()));
	        
	        startActivity(intent);
	        
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
		
        return false;
	}
	
	public boolean refresh_onClick(View v){
		
		updateSpecialOffer();
		
        return false;
	}

	private void updateSpecialOffer() {

		TextView title = (TextView) findViewById(R.id.special_offer_title);
		title.setText(Helper.SpecialOffer.getTitle());

        if(PersistanceManager.isConnectionAvailable(this)){
	        
	        ImageView icon = (ImageView) findViewById(R.id.special_offer_icon);

	        ImageLoader.getInstance().displayImage(Helper.SpecialOffer.getImageUrl(), icon);
        }
	}
}
