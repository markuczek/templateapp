package com.apps4u.templateapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.apps4u.templateapp.model.dto.News;
import com.apps4u.templateapp.viewmodel.Helper;
import com.apps4u.templateapp.viewmodel.NewsListAdapter;

public class NewsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		updateNewsContent();
	}

	public void updateNewsContent() {
		if (Helper.NewsList == null || Helper.NewsList.size() == 0)
			return;

		final News[] news = new News[Helper.NewsList.size()];
		Helper.NewsList.toArray(news);

		NewsListAdapter adapter = new NewsListAdapter(this, news);
		
		ListView scrollView = (ListView)findViewById(R.id.news_list);
		scrollView.setAdapter(adapter);
	}

}
