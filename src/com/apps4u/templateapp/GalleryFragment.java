package com.apps4u.templateapp;

import com.apps4u.templateapp.model.webservice.PersistanceManager;
import com.apps4u.templateapp.viewmodel.Helper;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class GalleryFragment extends Fragment {

    public static final String ARG_PAGE = "page";

	private static Activity mActivity;

    private int mPageNumber;

    public static GalleryFragment create(int pageNumber, Activity activity) {
    	mActivity = activity;
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public GalleryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_gallery, container, false);
    	
        ImageView image = (ImageView) rootView.findViewById(R.id.gallery_picture);

        ImageLoader.getInstance().displayImage(Helper.Images.get(mPageNumber), image);

        return rootView;
    }


    public int getPageNumber() {
        return mPageNumber;
    }
}
